At Marsalisi Law, law gets personal. If you come to us with a personal injury case, you won�t be passed off to an intern or paralegal. You will receive personal service from Frank Marsalisi.
Our caring goes beyond the intricacies and minutiae of legal parlance.

Address: 7035 Central Ave, Suite A, Saint Petersburg, FL 33710, USA

Phone: 727-800-5052